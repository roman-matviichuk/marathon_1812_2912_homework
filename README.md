# Game of Hearts

## About
This is part of the CyberBionic Systematics [Marathon](https://edu.cbsystematics.com/ua/webinars/hearts-in-python). 

The aim of the project, in addition to creating a card game [Hearts](https://bicyclecards.com/how-to-play/hearts/), is a theoretical and practical understanding of

- OOP in Python
- Type Annotations