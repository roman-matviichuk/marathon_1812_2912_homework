"""
The module contains the solutions to Tasks 1 & 2
"""


def get_character_frequency(input_str: str) -> dict:
    """
    Take a string as input and return a dictionary where

    - key is a character from the string,
    - value is the frequency of occurrence of the character in the string

    >>> get_character_frequency("")
    {}
    >>> get_character_frequency("a")
    {'a': 1.0}
    >>> get_character_frequency("11")
    {'1': 1.0}
    >>> d1 = get_character_frequency("mama"); {'m': d1['m'], 'a': d1['a']}
    {'m': 0.5, 'a': 0.5}
    >>> d2 = get_character_frequency("onion"); {'o': d2['o'], 'n': d2['n'], 'i': d2['i']}
    {'o': 0.4, 'n': 0.4, 'i': 0.2}

    """
    total = len(input_str)
    characters = set(input_str)
    return {
        char: input_str.count(char) / total for char in characters
    }


def evaluate_ternary_operator(x: int, y: int) -> str:
    """
    x < y --> x + y
    >>> evaluate_ternary_operator(1, 2)
    '3'

    x == y --> 0
    >>> evaluate_ternary_operator(1, 1)
    '0'

    x > y --> x - y
    >>> evaluate_ternary_operator(2, 1)
    '1'

    x == 0 та y == 0 --> game over
    >>> evaluate_ternary_operator(0, 0)
    'game over'

    """
    return str(
        x + y if x < y else
        x - y if x > y else
        "game over" if x == 0 and y == 0 else
        0
    )


if __name__ == "__main__":
    import doctest
    doctest.testmod(verbose=False)
