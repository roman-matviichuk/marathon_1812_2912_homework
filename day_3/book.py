"""
A set of classes to represent a Book
"""
from typing import Optional, List


class Book:
    def __init__(
            self,
            title: str,
            authors: List['Author'],
            description: Optional[str] = None,
            language: Optional[str] = None,
            genres: Optional[List['Genre']] = None,
            year_of_publication: Optional[int] = None,
            isbn: Optional[str] = None
    ) -> None:
        self.title = title
        self.authors = authors
        self.description = description
        self.language = language
        self.genres = genres
        self.year_of_publication = year_of_publication
        self.isbn = isbn

    def __str__(self):
        """
        >>> zelazny = Author('Roger', 'Zelazny')
        >>> saberhagen = Author('Fred', 'Saberhagen')

        >>> print(Book('Nine Princes in Amber', [zelazny]))
        Nine Princes in Amber by Roger Zelazny

        >>> print(Book('Coils', [zelazny, saberhagen]))
        Coils by Roger Zelazny, Fred Saberhagen

        """
        authors_str = ', '.join(f"{author!s}" for author in self.authors)
        return f"{self.title} by {authors_str}"

    def __repr__(self):
        """
        >>> zelazny = Author('Roger', 'Zelazny')
        >>> saberhagen = Author('Fred', 'Saberhagen')

        >>> Book('Nine Princes in Amber', [zelazny], language='EN',
        ... year_of_publication=1970, isbn='978-0380014309')
        Book('Nine Princes in Amber', [Author('Roger', 'Zelazny', None)], \
None, 'EN', None, 1970, '978-0380014309')

        >>> Book('Coils', [zelazny, saberhagen]) #doctest: +ELLIPSIS
        Book('Coils', [Author('Roger', 'Zelazny', None), Author('Fred', 'Saberhagen', None)], \
None, None, None, None, None)

        """
        return (
            f"Book('{self.title}', {self.authors}"
            + ", " + ('None' if self.description is None else f"'{self.description}'")
            + ", " + ('None' if self.language is None else f"'{self.language}'")
            + ", " + ('None' if self.genres is None else f"{self.genres}")
            + ", " + ('None' if self.year_of_publication is None else f"{self.year_of_publication}")
            + ", " + ('None' if self.isbn is None else f"'{self.isbn}'")
            + ")"
        )

    def __eq__(self, other: 'Book') -> bool:
        """
        >>> zelazny = Author('Roger', 'Zelazny')
        >>> saberhagen = Author('Fred', 'Saberhagen')

        >>> amber = Book('Nine Princes in Amber', [zelazny])
        >>> coils = Book('Coils', [zelazny, saberhagen])
        >>> amber == coils
        False

        >>> coils1 = Book('Coils', [zelazny, saberhagen], language='EN')
        >>> coils2 = Book('Coils', [zelazny, saberhagen], isbn='978-0380014309')
        >>> coils1 == coils2
        True

        """
        if not isinstance(other, Book):
            raise TypeError(
                f"'==' not supported between instances of '{type(self)}' and '{type(other)}'"
            )
        return all([
            self.title == other.title,
            set(self.authors) == set(other.authors)
        ])


class Author:
    def __init__(
            self,
            first_name: str,
            last_name: str,
            year_of_birth: Optional[int] = None
    ):
        self.first_name = first_name
        self.last_name = last_name
        self.year_of_birth = year_of_birth

    def __str__(self):
        """
        >>> print(Author('Arthur', 'Clarke', 1917))
        Arthur Clarke

        >>> print(Author('Ray', 'Bradbury'))
        Ray Bradbury

        """
        return f"{self.first_name} {self.last_name}"

    def __repr__(self):
        """
        >>> Author('Arthur', 'Clarke', 1917)
        Author('Arthur', 'Clarke', 1917)

        >>> Author('Ray', 'Bradbury')
        Author('Ray', 'Bradbury', None)

        """
        return (
            f"Author('{self.first_name}', '{self.last_name}', "
            + ('None' if self.year_of_birth is None else f"{self.year_of_birth}")
            + ")"
        )

    def __eq__(self, other: 'Author') -> bool:
        """
        >>> Author('Ray', 'Bradbury') == Author('Arthur', 'Clarke', 1917)
        False

        >>> Author('Ray', 'Bradbury', 1920) == Author('Ray', 'Bradbury', 1920)
        True

        >>> Author('Ray', 'Bradbury', 1920) == Author('Ray', 'Bradbury')
        False

        """
        if not isinstance(other, Author):
            raise TypeError(
                f"'==' not supported between instances of '{type(self)}' and '{type(other)}'"
            )
        return all([
            self.first_name == other.first_name,
            self.last_name == other.last_name,
            self.year_of_birth == other.year_of_birth
        ])

    def __hash__(self):
        return hash((self.first_name, self.last_name, self.year_of_birth))


class Genre:
    def __init__(
            self,
            name: str,
            description: Optional[str] = None
    ) -> None:
        self.name = name
        self.description = description

    def __str__(self) -> str:
        """
        >>> print(Genre('sci-fi', 'Science Fiction'))
        sci-fi

        >>> print(Genre('non-fiction'))
        non-fiction

        """
        return self.name

    def __repr__(self):
        """
        >>> Genre('sci-fi', 'Science Fiction')
        Genre('sci-fi', 'Science Fiction')

        >>> Genre('non-fiction')
        Genre('non-fiction', None)

        """
        return (
            f"Genre('{self.name}', "
            + ('None' if self.description is None else f"'{self.description}'")
            + ")"
        )


if __name__ == '__main__':
    import doctest
    doctest.testmod(verbose=False)
