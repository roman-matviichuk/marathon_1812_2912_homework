"""
A set of classes to represent a Book
"""
from datetime import datetime
from typing import Optional, List


class Book:
    def __init__(
            self,
            title: str,
            language: str,
            year_of_publication: int,
            *authors: 'Author',
            description: Optional[str] = None,
            genres: Optional[List['Genre']] = None,
            isbn: Optional[str] = None
    ) -> None:
        self.title = title
        self.language = language
        self.year_of_publication = year_of_publication
        self.authors = list(authors)
        self.description = description
        self.genres = genres
        self.isbn = isbn

    def __str__(self):
        """
        >>> zelazny = Author('Roger', 'Zelazny')
        >>> saberhagen = Author('Fred', 'Saberhagen')

        >>> print(Book('Nine Princes in Amber', 'EN', 1970, zelazny))
        Nine Princes in Amber by Roger Zelazny

        >>> print(Book('Coils', 'EN', 1982, zelazny, saberhagen))
        Coils by Roger Zelazny, Fred Saberhagen

        """
        authors_str = ', '.join(f"{author!s}" for author in self.authors)
        return f"{self.title} by {authors_str}"

    def __repr__(self):
        """
        >>> zelazny = Author('Roger', 'Zelazny')
        >>> saberhagen = Author('Fred', 'Saberhagen')

        >>> Book('Nine Princes in Amber', 'EN', 1970, zelazny, isbn='978-0380014309')
        Book('Nine Princes in Amber', 'EN', 1970, Author('Roger', 'Zelazny', None), \
description=None, genres=None, isbn='978-0380014309')

        >>> Book('Coils', 'EN', 1982, zelazny, saberhagen)
        Book('Coils', 'EN', 1982, Author('Roger', 'Zelazny', None), \
Author('Fred', 'Saberhagen', None), description=None, genres=None, isbn=None

        """
        return (
            f"Book('{self.title}', '{self.language}', "
            f"{self.year_of_publication}, {str(self.authors)[1:-1]}, "
            f"description=" + ('None' if self.description is None else f"'{self.description}'")
            + ", genres=" + ('None' if self.genres is None else f"{self.genres}")
            + ", isbn=" + ('None' if self.isbn is None else f"'{self.isbn}')")
        )

    def __eq__(self, other: 'Book') -> bool:
        """
        >>> zelazny = Author('Roger', 'Zelazny')
        >>> saberhagen = Author('Fred', 'Saberhagen')

        >>> amber = Book('Nine Princes in Amber', 'EN', 1970, zelazny)
        >>> coils = Book('Coils', 'EN', 1982, zelazny, saberhagen)
        >>> amber == coils
        False

        >>> coils1 = Book('Coils', 'EN', 1982, zelazny, saberhagen)
        >>> coils2 = Book('Coils', 'UA', 2024, zelazny, saberhagen)
        >>> coils1 == coils2
        True

        """
        if not isinstance(other, Book):
            raise TypeError(
                f"'==' not supported between instances of '{type(self)}' and '{type(other)}'"
            )
        return all([
            self.title == other.title,
            set(self.authors) == set(other.authors)
        ])

    @property
    def age(self):
        """
        Age of the book in years

        >>> zelazny = Author('Roger', 'Zelazny')
        >>> Book('Nine Princes in Amber', 'EN', 1970, zelazny).age
        53

        """
        return datetime.now().year - self.year_of_publication


class Author:
    def __init__(
            self,
            first_name: str,
            last_name: str,
            year_of_birth: Optional[int] = None
    ):
        self.first_name = first_name
        self.last_name = last_name
        self.year_of_birth = year_of_birth

    def __str__(self):
        """
        >>> print(Author('Arthur', 'Clarke', 1917))
        Arthur Clarke

        >>> print(Author('Ray', 'Bradbury'))
        Ray Bradbury

        """
        return f"{self.first_name} {self.last_name}"

    def __repr__(self):
        """
        >>> Author('Arthur', 'Clarke', 1917)
        Author('Arthur', 'Clarke', 1917)

        >>> Author('Ray', 'Bradbury')
        Author('Ray', 'Bradbury', None)

        """
        return (
            f"Author('{self.first_name}', '{self.last_name}', "
            + ('None' if self.year_of_birth is None else f"{self.year_of_birth}")
            + ")"
        )

    def __eq__(self, other: 'Author') -> bool:
        """
        >>> Author('Ray', 'Bradbury') == Author('Arthur', 'Clarke', 1917)
        False

        >>> Author('Ray', 'Bradbury', 1920) == Author('Ray', 'Bradbury', 1920)
        True

        >>> Author('Ray', 'Bradbury', 1920) == Author('Ray', 'Bradbury')
        False

        """
        if not isinstance(other, Author):
            raise TypeError(
                f"'==' not supported between instances of '{type(self)}' and '{type(other)}'"
            )
        return all([
            self.first_name == other.first_name,
            self.last_name == other.last_name,
            self.year_of_birth == other.year_of_birth
        ])

    def __hash__(self):
        return hash((self.first_name, self.last_name, self.year_of_birth))


class Genre:
    def __init__(
            self,
            name: str,
            description: Optional[str] = None
    ) -> None:
        self.name = name
        self.description = description

    def __str__(self) -> str:
        """
        >>> print(Genre('sci-fi', 'Science Fiction'))
        sci-fi

        >>> print(Genre('non-fiction'))
        non-fiction

        """
        return self.name

    def __repr__(self):
        """
        >>> Genre('sci-fi', 'Science Fiction')
        Genre('sci-fi', 'Science Fiction')

        >>> Genre('non-fiction')
        Genre('non-fiction', None)

        """
        return (
            f"Genre('{self.name}', "
            + ('None' if self.description is None else f"'{self.description}'")
            + ")"
        )


if __name__ == '__main__':
    import doctest
    doctest.testmod(verbose=False)
